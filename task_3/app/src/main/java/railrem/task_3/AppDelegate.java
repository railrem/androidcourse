package railrem.task_3;

import android.app.Application;

import com.facebook.stetho.Stetho;

import railrem.task_3.chucknorris.api.ApiFactory;

/**
 * @author Timur Valiev
 */
public class AppDelegate extends Application {

    @Override
    public void onCreate() {
        super.onCreate();

        ApiFactory.provideClient();
        if (BuildConfig.DEBUG) {
            Stetho.initializeWithDefaults(this);
        }
    }
}
