package railrem.task_3.chucknorris.api;

import java.util.List;

import railrem.task_3.chucknorris.models.Joke;
import railrem.task_3.chucknorris.models.JokeBox;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * @author Timur Valiev
 */
public interface JokeService {

    @GET("random")
    Call<Joke> getRandomJoke();

    @GET("categories")
    Call<List<String>> getAvailableCategories();

    @GET("search")
    Call<JokeBox> searchJokeByText(@Query("query") String name);

}
