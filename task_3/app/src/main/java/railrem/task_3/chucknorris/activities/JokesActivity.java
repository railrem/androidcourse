package railrem.task_3.chucknorris.activities;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import railrem.task_3.R;
import railrem.task_3.chucknorris.api.ApiFactory;
import railrem.task_3.chucknorris.api.JokeService;
import railrem.task_3.chucknorris.models.Joke;
import railrem.task_3.chucknorris.models.JokeBox;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * @author Timur Valiev
 */
public class JokesActivity extends AppCompatActivity {

    private RecyclerView jokeList;

    private JokeService jokeService;

    private LinearLayoutManager mLinearLayoutManager;

    private ProgressDialog loadingDialog;

    private EditText jokeText;
    private Button filterButton;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_jokes);

        initViews();
        loadingDialog = ProgressDialog.show(this, "", getString(R.string.network_loading), true);

        // Создаем сервис через который будет выполняться запрос
        jokeService = ApiFactory.getRetrofitInstance().create(JokeService.class);
        fetchJokesByText("gun");
        filterButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fetchJokesByText(jokeText.getText().toString());
            }
        });
    }

    private void initViews() {
        jokeList = (RecyclerView) findViewById(R.id.jokes);
        mLinearLayoutManager = new LinearLayoutManager(this);
        jokeList.setLayoutManager(mLinearLayoutManager);
        filterButton = (Button) findViewById(R.id.searchBtn);
        jokeText = (EditText) findViewById(R.id.jokeText);
    }

    private void fetchJokesByText(String text) {
        // Создаем экземпляр запроса со всем необходимыми настройками
        Call<JokeBox> call = jokeService.searchJokeByText(text);

        // Отображаем progress bar
        loadingDialog.show();

        // Выполняем запрос асинхронно
        call.enqueue(new Callback<JokeBox>() {

            // В случае если запрос выполнился успешно, то мы переходим в метод onResponse(...)
            @Override
            public void onResponse(@NonNull Call<JokeBox> call, @NonNull Response<JokeBox> response) {
                if (response.isSuccessful()) {
                    // Если в ответ нам пришел код 2xx, то отображаем содержимое запроса
                    fillJokes(response.body().getResult());

                } else {
                    // Если пришел код ошибки, то обрабатываем её
                    Toast.makeText(JokesActivity.this, R.string.network_error, Toast.LENGTH_SHORT).show();
                }

                // Скрываем progress bar
                loadingDialog.dismiss();
            }

            // Если запрос не удалось выполнить, например, на телефоне отсутствует подключение к интернету
            @Override
            public void onFailure(@NonNull Call<JokeBox> call, @NonNull Throwable t) {
                // Скрываем progress bar
                loadingDialog.dismiss();

                Toast.makeText(JokesActivity.this, R.string.network_error, Toast.LENGTH_SHORT).show();
                Log.d("Error", t.getMessage());
            }
        });
    }


    private void fillJokes(@NonNull ArrayList<Joke> jokes) {
        ArrayList<Joke> limitedJokes = new ArrayList<>();
        Integer size = 30;
        if (jokes.size() < 30) {
            size = jokes.size();
        }
        for (int i = 0; i < size; i++) {
            Joke joke = jokes.get(i);
            joke.setValue(joke.getValue().replaceAll("Chuck Norris", "Rail Khisa"));
            limitedJokes.add(joke);
        }
        JokeAdapter jokeAdapter = new JokeAdapter(limitedJokes, this);
        jokeList.setAdapter(jokeAdapter);
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(jokeList.getContext(),
                mLinearLayoutManager.getOrientation());
        jokeList.addItemDecoration(dividerItemDecoration);
    }


    public class JokeViewHolder extends RecyclerView.ViewHolder {
        public TextView value;
        public ImageView icon;


        public JokeViewHolder(View view) {
            super(view);
            value = (TextView) itemView.findViewById(android.R.id.text1);
            icon = (ImageView) itemView.findViewById(android.R.id.icon);
        }
    }

    public class JokeAdapter extends RecyclerView.Adapter<JokeViewHolder> {
        private ArrayList<Joke> models;
        private Context ctx;

        public JokeAdapter(ArrayList<Joke> myDataset, Context ctx) {
            models = myDataset;
            this.ctx = ctx;
        }

        @Override
        public int getItemCount() {
            return models.size();
        }

        @Override
        public void onBindViewHolder(JokeViewHolder holder, int position) {
            Joke joke = models.get(position);
            Picasso.with(ctx)
                    .load(joke.getIconUrl())
                    .into(holder.icon);
            holder.value.setText(joke.getValue());
        }

        @Override
        public JokeViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            LayoutInflater layoutInflate = LayoutInflater.from(parent.getContext());
            View view = layoutInflate.inflate(android.R.layout.activity_list_item, parent, false);
            return new JokeViewHolder(view);
        }

    }
}
