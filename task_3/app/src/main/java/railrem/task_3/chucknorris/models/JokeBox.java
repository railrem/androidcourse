package railrem.task_3.chucknorris.models;

/**
 * Created by railr on 19.08.2017.
 */

import java.util.ArrayList;

public class JokeBox {

    private Integer total;

    private ArrayList<Joke> result;

    public Integer getTotal() {
        return total;
    }

    public void setTotal(Integer total) {
        this.total = total;
    }

    public ArrayList<Joke> getResult() {
        return result;
    }

    public void setResult(ArrayList<Joke> result) {
        this.result = result;
    }
}
