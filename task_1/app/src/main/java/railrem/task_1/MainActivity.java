package railrem.task_1;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.app.AppCompatCallback;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;

public class MainActivity extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button button = (Button) findViewById(R.id.calc);
        button.setOnClickListener(new MoveActionButtonListener(MainActivity.this, CalcActivity.class));

        Button buttonProfile = (Button) findViewById(R.id.profile);
        buttonProfile.setOnClickListener(new MoveActionButtonListener(MainActivity.this, ProfileActivity.class));
    }

    class MoveActionButtonListener implements View.OnClickListener {
        Context prevCntx;
        Class<?> cls;

        public MoveActionButtonListener(Context prevCntx, Class<?> cls) {
            this.prevCntx = prevCntx;
            this.cls = cls;
        }

        @Override
        public void onClick(View v) {
            Intent intent = new Intent(prevCntx, cls);
            startActivity(intent);
        }
    }

}
