package railrem.task_1;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ListView;

/**
 * Created by railr on 14.08.2017.
 */

public class ProfileActivity extends AppCompatActivity {
    Integer[] imageIds = {
            R.drawable.account,
            R.drawable.accountmultiple,
            R.drawable.chartline,
            R.drawable.pica
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.profile);

        // получаем экземпляр элемента ListView
        ListView listView = (ListView) findViewById(R.id.list1);

        // определяем массив типа String
        final String[] menuItems = getResources().getStringArray(R.array.menu_items);

        // используем адаптер данных
        CustomListAdaptor adapter = new CustomListAdaptor(this, menuItems, imageIds);

        listView.setAdapter(adapter);
        listView.setDivider(null);
    }
}
