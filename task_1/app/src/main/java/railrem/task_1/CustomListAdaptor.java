package railrem.task_1;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;


public class CustomListAdaptor extends ArrayAdapter<String> {
    private final Activity context;
    private final String[] texts;
    private final Integer[] imageIds;

    public CustomListAdaptor(Activity context,String[] texts, Integer[] imageIds) {
        super(context, R.layout.custom_list_item, texts);
        this.context = context;
        this.texts = texts;
        this.imageIds = imageIds;
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {
        LayoutInflater inflater = context.getLayoutInflater();
        View rowView = inflater.inflate(R.layout.custom_list_item, null, true);
        TextView txtTitle = (TextView) rowView.findViewById(R.id.rtext);

        ImageView imageView = (ImageView) rowView.findViewById(R.id.ricon);
        txtTitle.setText(texts[position]);

        imageView.setImageResource(imageIds[position]);
        return rowView;
    }
}
