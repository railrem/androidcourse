package railrem.task_2;

import java.util.Random;

/**
 * Created by railr on 12.08.2017.
 */

public class CapitalModal {
    public String countyName;
    public Integer population;
    public Integer imageId;

    public CapitalModal(String county,Integer imageId) {
        this.countyName = county;
        Random r = new Random();
        this.population = r.nextInt(100)*1000;
        this.imageId = imageId;
    }
}
