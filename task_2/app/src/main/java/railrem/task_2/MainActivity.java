package railrem.task_2;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button button = (Button) findViewById(R.id.recycleview);
        button.setOnClickListener(new MoveActionButtonListener(this, RecycleviewActivity.class));

    }

    class MoveActionButtonListener implements View.OnClickListener {
        Context prevCntx;
        Class<?> cls;

        public MoveActionButtonListener(Context prevCntx, Class<?> cls) {
            this.prevCntx = prevCntx;
            this.cls = cls;
        }

        @Override
        public void onClick(View v) {
            Intent intent = new Intent(prevCntx, cls);
            startActivity(intent);
        }
    }
}
