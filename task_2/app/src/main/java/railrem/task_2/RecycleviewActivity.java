package railrem.task_2;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import java.util.ArrayList;

/**
 * Created by railr on 14.08.2017.
 */

public class RecycleviewActivity extends AppCompatActivity {

    private RecyclerView countryList;
    private LinearLayoutManager mLinearLayoutManager;
    Integer[] imageId = {
            R.drawable.country_afghanistan,
            R.drawable.country_aland,
            R.drawable.country_albania,
            R.drawable.country_algeria,
            R.drawable.country_andorra,
            R.drawable.country_angola,
            R.drawable.country_anguilla,
            R.drawable.country_antarctica,
            R.drawable.country_antigua,
            R.drawable.country_armenia,
            R.drawable.country_aruba,
            R.drawable.country_australia,
            R.drawable.country_austria,
            R.drawable.country_azerbaijan,
            R.drawable.country_bahamas,
            R.drawable.country_bahrain,
            R.drawable.country_bangladesh,
    };
    String[] countries = {
            "Афганистан",
            "Алания",
            "Албания",
            "Алжир",
            "Андора",
            "Ангола",
            "Англия",
            "Андарктида",
            "Антигуа",
            "Армения",
            "Аруба",
            "Австралия",
            "Австрия",
            "Азербайджан",
            "Багамы",
            "Бахран",
            "Бангладеш",
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.recycle_view);

        countryList = (RecyclerView) findViewById(R.id.recycle_view);
        mLinearLayoutManager = new LinearLayoutManager(this);
        countryList.setLayoutManager(mLinearLayoutManager);
        ArrayList<CapitalModal> models = new ArrayList<>();
        for (int index = 0; index < imageId.length; index++) {
            models.add(new CapitalModal(countries[index], imageId[index]));
        }
        CountryAdapter countryAdapter = new CountryAdapter(models);
        countryList.setAdapter(countryAdapter);
    }


    public class CountryViewHolder extends RecyclerView.ViewHolder {
        public TextView countryName;
        public TextView population;
        public ImageView icon;


        public CountryViewHolder(View view) {
            super(view);
            countryName = (TextView) itemView.findViewById(R.id.country_name);
            population = (TextView) itemView.findViewById(R.id.population);
            icon = (ImageView) itemView.findViewById(R.id.country_icon);
        }
    }

    public class CountryAdapter extends RecyclerView.Adapter<CountryViewHolder> {
        private ArrayList<CapitalModal> models;

        public CountryAdapter(ArrayList<CapitalModal> myDataset) {
            models = myDataset;
        }

        @Override
        public int getItemCount() {
            return models.size();
        }

        @Override
        public void onBindViewHolder(CountryViewHolder holder, int position) {
            CapitalModal model = models.get(position);
            holder.countryName.setText(model.countyName.concat("-"));
            holder.population.setText(Integer.toString(model.population));
            holder.icon.setImageResource(model.imageId);
        }

        @Override
        public CountryViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            LayoutInflater layoutInflate = LayoutInflater.from(parent.getContext());
            View view = layoutInflate.inflate(R.layout.country_item, parent, false);
            return new CountryViewHolder(view);
        }

    }

}
